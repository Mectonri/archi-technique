package fr.intech.FlightEngine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightEngineApplication.class, args);
	}

}
