package fr.intech.FlightEngine.service;

import java.util.List;

import fr.intech.FlightEngine.dto.ExternalFlights;

public interface ExternalAPIService {

	List<ExternalFlights> getAllFlights();
	
}
