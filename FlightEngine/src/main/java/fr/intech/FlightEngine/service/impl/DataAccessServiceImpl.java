package fr.intech.FlightEngine.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import fr.intech.FlightEngine.dto.Flights;
import fr.intech.FlightEngine.service.DataAccessService;

@Component
public class DataAccessServiceImpl implements DataAccessService {
	

	private static final String URL = "http://localhost:8084/api/DAS/flights";
	
	@Override
	public List<Flights> getAllFlights() throws Exception {
		
		URL url = new URL(URL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();
		JSONArray jsonArr = new JSONArray(content.toString());
		
		ArrayList<Flights> flightList = new ArrayList<Flights>();
		
		
		for (int i = 0; i < jsonArr.length(); i++) {
            flightList.add(Flights.fromJson(jsonArr.getJSONObject(i)));
        }
		
		
		//TODO appel externalserviceapi 
		
		return flightList;
	}

	@Override
	public List<Flights> getFlightByDate(String date) throws Exception {
		
		URL url = new URL(URL.concat("/").concat(date));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();
		JSONArray jsonArr = new JSONArray(content.toString());
		
		ArrayList<Flights> flightList = new ArrayList<Flights>();
		
		
		for (int i = 0; i < jsonArr.length(); i++) {
          flightList.add(Flights.fromJson(jsonArr.getJSONObject(i)));
		}
		
		return flightList;
	}


}
