package fr.intech.FlightEngine.service;

import java.util.List;

import fr.intech.FlightEngine.dto.Flights;

public interface DataAccessService {

	List<Flights> getAllFlights() throws Exception;

	List<Flights> getFlightByDate(String date) throws Exception;

}
