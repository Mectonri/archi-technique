package fr.intech.FlightEngine.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.intech.FlightEngine.dto.Flights;
import fr.intech.FlightEngine.service.DataAccessService;


@CrossOrigin
@RestController
@RequestMapping("/flight")
public class FlightController {

	@Autowired
	DataAccessService dataAccessService;
	
	@GetMapping("/")
	public ResponseEntity<List<Flights>> getAllRoutes() throws Exception{
		
		return ResponseEntity.ok(dataAccessService.getAllFlights());
	}
	
	@GetMapping("/{date}")
	public ResponseEntity<List<Flights>> getFlightByDate(@PathVariable(value = "date") String date) throws Exception{
		
		return ResponseEntity.ok(dataAccessService.getFlightByDate(date));
	}
	
}
