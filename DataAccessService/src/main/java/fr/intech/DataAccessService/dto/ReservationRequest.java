package fr.intech.DataAccessService.dto;

import org.json.JSONObject;

public class ReservationRequest {

	private String flightDate;
	
	public long getIdRoute() {
		return idRoute;
	}
	public void setIdRoute(long idRoute) {
		this.idRoute = idRoute;
	}
	private long idRoute;
	 
	public String getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}
	public Object toJson() {
		JSONObject json = new JSONObject();
        json.put("idRoute", this.idRoute);
        json.put("flightDate", this.flightDate);
        return json;
	}
}
