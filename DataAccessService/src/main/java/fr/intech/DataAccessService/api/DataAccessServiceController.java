package fr.intech.DataAccessService.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.intech.DataAccessService.business.Currency;
import fr.intech.DataAccessService.business.Routes;
import fr.intech.DataAccessService.dto.ConfirmReservationRequest;
import fr.intech.DataAccessService.dto.ReservationRequest;
import fr.intech.DataAccessService.service.CurrencyService;
import fr.intech.DataAccessService.service.ReservationService;
import fr.intech.DataAccessService.service.RoutesService;


@CrossOrigin
@RestController
@RequestMapping("/DAS")
public class DataAccessServiceController {

	@Autowired
	RoutesService routesService;
	
	@Autowired 
	CurrencyService currencyService;
	
	@Autowired 
	ReservationService reservationService;
	
	@GetMapping("/flights")
	public ResponseEntity<List<Routes>> getAllRoutes(){
		return ResponseEntity.ok(routesService.getAllFlights());
	}
	
	@GetMapping("/currencies")
	public ResponseEntity<List<Currency>> getAllCurrencies(){
		return ResponseEntity.ok(currencyService.getCurrenciesRates());
	}
	
	@GetMapping("/flights/{date}")
	public ResponseEntity<List<Routes>> getFlightsByDate(@PathVariable(value = "date") String date){
		return ResponseEntity.ok(routesService.getFlightsByDate(date));
	}
	
	@PostMapping("/make")
	public ResponseEntity<Long> makeReservation(@RequestBody ReservationRequest request)throws Exception{
		
		Long reservationId = reservationService.makeReservation(request);
		return ResponseEntity.ok(reservationId);
	}
	
	@PostMapping("/confirm")
	public void confirmReservation(@RequestBody ConfirmReservationRequest request)throws Exception{
		reservationService.confirmReservation(request);
	}
	
	@PostMapping("/delete")
	 public ResponseEntity<?> deleteReservation(@RequestBody long idReserv) throws Exception {
		 
		reservationService.deleteReservation(idReserv);
		return new ResponseEntity<>(HttpStatus.OK);
		 
	 }
}
