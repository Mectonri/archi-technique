package fr.intech.DataAccessService.service.impl;

import org.springframework.stereotype.Component;

import fr.intech.DataAccessService.business.Currency;
import fr.intech.DataAccessService.service.CurrencyService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Component
public class CurrencyServiceImpl implements CurrencyService {

    private List<Currency> CurrencyRatesList = new ArrayList<Currency>();
    private LocalDate LastUpdateDay;



    @Override
    public List<Currency> getCurrenciesRates(){
        LocalDate currDate = LocalDate.now();
        if (LastUpdateDay == null || LastUpdateDay.isBefore(currDate)) {
            String result;

            try {
                URL url = new URL("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
                InputStream input= url.openStream();
                result = new String(input.readAllBytes(), StandardCharsets.UTF_8);
            } catch (Exception e) {
                return null;
            }
                BufferedReader bufReader = new BufferedReader(new StringReader(result));

                String line=null;

                LastUpdateDay = currDate;
                CurrencyRatesList.clear();
                while(true)
                {
                    try {
                        if (!((line=bufReader.readLine()) != null)) break;
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    int currencyStart = line.indexOf("currency=\'");
                    if (currencyStart != -1) {
                        int rateStart = line.indexOf("rate=\'") + 6;
                        int endOfLine = line.indexOf("\'/>");
                        currencyStart = currencyStart + 10;
                        String currencyName = line.substring(currencyStart, currencyStart + 3);
                        String currencyRate = line.substring(rateStart, endOfLine);
                        Currency curr = new Currency(currencyName,Double.parseDouble(currencyRate));
                        CurrencyRatesList.add(curr);
                    }
                }
        }
        return CurrencyRatesList;
    }
}
