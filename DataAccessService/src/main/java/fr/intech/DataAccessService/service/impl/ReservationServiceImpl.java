package fr.intech.DataAccessService.service.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.intech.DataAccessService.business.Reservation;
import fr.intech.DataAccessService.dto.ConfirmReservationRequest;
import fr.intech.DataAccessService.dto.ReservationRequest;
import fr.intech.DataAccessService.service.ReservationService;
import fr.intech.DataAccessService.service.RoutesService;

@Component
public class ReservationServiceImpl implements ReservationService{

	@Autowired 
	RoutesService routesService;
	
	@Override
	public Long makeReservation(ReservationRequest request) throws Exception {
		String flightDate = request.getFlightDate();
		Long routId = request.getIdRoute();
		
		
		
		if(!isCapacityReached(routId, flightDate)) {
			List<Reservation> reservationList = getAllReservations();
			
			Reservation reserv = new Reservation();
			reserv.setFlightDate(flightDate);
			reserv.setId(getLastReservationId());
			reserv.setRoutes(routesService.getRouteById(routId));
			
			reservationList.add(reserv);
			
			
			JSONArray reservationArray = new JSONArray();
			for (Reservation reservation :  reservationList) {
				reservationArray.put(reservation.toJson(false));
	        }
			
			try (FileWriter writer = new FileWriter("src/main/resources/reservations.json")) {
				writer.write(reservationArray.toString());
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return reserv.getId();
			
		}else {
			throw new Exception("Capacity for this route on this date has been reached.");
		}
	}

	@Override
	public int getReservationNumberForDate(String flightDate) {
		ArrayList<Reservation> reservationList = new ArrayList<Reservation>();
		
		InputStream inputStream = RoutesServiceImpl.class.getResourceAsStream("/reservations.json");

        Scanner scanner = new Scanner(inputStream);
        StringBuilder jsonContent = new StringBuilder();

        while (scanner.hasNextLine()) {
            jsonContent.append(scanner.nextLine());
        }

        if(jsonContent.length() == 0) {
        	scanner.close();
        	return 0;
        }
        JSONArray jsonArray = new JSONArray(jsonContent.toString());

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Reservation reservation = Reservation.fromJson(jsonObject);
            if(reservation.getFlightDate() == flightDate) {
            	reservationList.add(reservation);
            }
            
        }
		
		scanner.close();
		
		return reservationList.size();
	}

	@Override
	public boolean isCapacityReached(Long idRoute, String flightDate) {
		int placeReserved = getReservationNumberForDate(flightDate);
		
		if(placeReserved < routesService.getRouteById(idRoute).getCapacity()) {
			return false;
		}
		return true;
	}

	@Override
	public Long getLastReservationId() {
		Long highestId = 0L;
		List<Reservation> reservations = getAllReservations();
		
		for(Reservation reserv : reservations) {
			if (reserv.getId() > highestId) {
				highestId = reserv.getId();
			}
		}
		
		return highestId + 1;
	}

	@Override
	public List<Reservation> getAllReservations() {
		ArrayList<Reservation> reservationList = new ArrayList<Reservation>();
		
		
		InputStream inputStream = RoutesServiceImpl.class.getResourceAsStream("/reservations.json");

        Scanner scanner = new Scanner(inputStream);
        StringBuilder jsonContent = new StringBuilder();

        while (scanner.hasNextLine()) {
            jsonContent.append(scanner.nextLine());
        }
        if (jsonContent.length() == 0) {
        	scanner.close();	        
    		return reservationList;
        }
        JSONArray jsonArray = new JSONArray(jsonContent.toString());

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            reservationList.add(Reservation.fromJson(jsonObject));
        }
		
		scanner.close();      
		return reservationList;
	}

	@Override
	public void confirmReservation(ConfirmReservationRequest request) {
		
		List<Reservation> reservationList = getAllReservations();
		
		
		JSONArray reservationArray = new JSONArray();
		for(Reservation reservation : reservationList) {
			
			if(reservation.getId() == request.getIdReservation()) {
				reservation.setNbrTickets(request.getNbrTickets());
				reservation.setFinalPrice(request.getFinalPrice());
				reservation.setOptions(request.getNbrLuggages());
			}
			reservationArray.put(reservation.toJson(false));
		}
		
		try (FileWriter writer = new FileWriter("src/main/resources/reservations.json")) {
			writer.write(reservationArray.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void deleteReservation(long idReserv) {
		
		List<Reservation> reservationList = getAllReservations();
		
		JSONArray reservationArray = new JSONArray();
		for(Reservation reservation : reservationList) {
			
			if(reservation.getId() != idReserv) {
				reservationArray.put(reservation.toJson(false));
			}
			
		}
		
		try (FileWriter writer = new FileWriter("src/main/resources/reservations.json")) {
			writer.write(reservationArray.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}

