package fr.intech.DataAccessService.service;

import java.util.List;

import fr.intech.DataAccessService.business.Currency;

public interface CurrencyService {
    List<Currency> getCurrenciesRates();
}
