package fr.intech.DataAccessService.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import fr.intech.DataAccessService.business.Routes;
import fr.intech.DataAccessService.service.RoutesService;

@Component
public class RoutesServiceImpl implements RoutesService{

	@Override
	public List<Routes> getAllFlights() {
		ArrayList<Routes> routesList = new ArrayList<Routes>();
		
		InputStream inputStream = RoutesServiceImpl.class.getResourceAsStream("/routes.json");

        Scanner scanner = new Scanner(inputStream);
        StringBuilder jsonContent = new StringBuilder();

        while (scanner.hasNextLine()) {
            jsonContent.append(scanner.nextLine());
        }

        JSONArray jsonArray = new JSONArray(jsonContent.toString());

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            routesList.add(Routes.fromJson(jsonObject));
        }
		
		scanner.close();	        
		return routesList;
	}

	@Override
	public Routes getRouteById(Long routId) {
		
		InputStream inputStream = RoutesServiceImpl.class.getResourceAsStream("/routes.json");
		Scanner scanner = new Scanner(inputStream);
	    StringBuilder jsonContent = new StringBuilder();
	    
	    while (scanner.hasNextLine()) {
	        jsonContent.append(scanner.nextLine());
	    }
	    
	    JSONArray jsonArray = new JSONArray(jsonContent.toString());
	    for (int i = 0; i < jsonArray.length(); i++) {
	        JSONObject jsonObject = jsonArray.getJSONObject(i);
	        Routes route = Routes.fromJson(jsonObject);
	        System.out.println(route.getId());
	        if (route.getId() == routId) {
	            scanner.close();
	            return route;
	        }
	    }

	    scanner.close();
	    return null;
	}

	@Override
	public List<Routes> getFlightsByDate(String date) {

		ArrayList<Routes> routesList = new ArrayList<Routes>();
		
		InputStream inputStream = RoutesServiceImpl.class.getResourceAsStream("/routes.json");

        Scanner scanner = new Scanner(inputStream);
        StringBuilder jsonContent = new StringBuilder();

        while (scanner.hasNextLine()) {
            jsonContent.append(scanner.nextLine());
        }

        JSONArray jsonArray = new JSONArray(jsonContent.toString());

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            routesList.add(Routes.fromJson(jsonObject));
        }
		
		scanner.close();	        
		return routesList;
	}
}
