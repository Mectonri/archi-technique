package fr.intech.DataAccessService.service;

import java.util.List;

import fr.intech.DataAccessService.business.Reservation;
import fr.intech.DataAccessService.dto.ConfirmReservationRequest;
import fr.intech.DataAccessService.dto.ReservationRequest;

public interface ReservationService {

	Long makeReservation(ReservationRequest request) throws Exception;
	
	Long getLastReservationId();
	
	List<Reservation> getAllReservations();
	
	int getReservationNumberForDate(String flightDate);
	
	boolean isCapacityReached(Long idRoute, String flightDate);

	void confirmReservation(ConfirmReservationRequest request);

	void deleteReservation(long idReserv);
}
