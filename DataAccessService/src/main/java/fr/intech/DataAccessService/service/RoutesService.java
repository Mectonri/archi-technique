package fr.intech.DataAccessService.service;

import java.util.List;

import fr.intech.DataAccessService.business.Routes;

public interface RoutesService {

	List<Routes> getAllFlights();

	Routes getRouteById(Long routId);

	List<Routes> getFlightsByDate(String date);

}
