package fr.intech.DataAccessService.business;

import org.json.JSONObject;

public class Options {

	private Long idOption;
	
	private String name; 
	
	private int price;

	public Options(Long id, String name, int price) {
		super();
		this.idOption = id;
		this.name = name;
		this.price = price;
	}
	
	public Options() {
		
	}

	public Long getId() {
		return idOption;
	}

	public void setId(Long id) {
		this.idOption = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public static Options fromJson(JSONObject json) {
        Long id = json.getLong("idOption");
        String name = json.getString("name");
        int price = json.getInt("price");

        return new Options(id, name, price);
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("idOption", this.idOption);
        json.put("name", this.name);
        json.put("price", this.price);

        return json;
    }



}
