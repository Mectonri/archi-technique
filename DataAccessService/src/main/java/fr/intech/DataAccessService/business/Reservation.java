package fr.intech.DataAccessService.business;

import org.json.JSONObject;

public class Reservation {

	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	private Routes routes;
	
	private String flightDate;
	
	private int nbrLuggage;
	
	private double finalPrice;
	
	private int nbrTickets;

	public Reservation(Long id, String firstName, String lastName, Routes routes, String flightDate, int nbrLuggage, double finalPrice, int nbrTickets) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.routes = routes;
		this.flightDate = flightDate;
		this.nbrLuggage = nbrLuggage;
		this.finalPrice = finalPrice;
		this.nbrTickets = nbrTickets;
	}
	
	public Reservation() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Routes getRoutes() {
		return routes;
	}

	public void setRoutes(Routes routes) {
		this.routes = routes;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public int getOptions() {
		return nbrLuggage;
	}

	public void setOptions(int nbrLuggage) {
		this.nbrLuggage = nbrLuggage;
	}
		
	public static Reservation fromJson(JSONObject json) {
        Long id = json.getLong("id");
        String firstName= json.has("firstName") ? json.getString("firstName") :"";
        String lastName = json.has("lastName") ?  json.getString("lastName"): "";
        Routes routes = Routes.fromJson(json.getJSONObject("routes"));
        String flightDate = json.getString("flightDate");
        int nbrLuggage = json.has("nbrLuggage") ? json.getInt("nbrLuggage") : 0;
        Double finalPrice = json.has("finalPrice") ? json.getDouble("finalPrice") : 0;
        int nbrTickets = json.has("nbrTickets")? json.getInt("nbrTickets"): 0;
        return new Reservation(id, firstName, lastName, routes, flightDate, nbrLuggage, finalPrice, nbrTickets);
    }

    public JSONObject toJson(boolean isOption) {
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("firstName", this.firstName);
        json.put("lastName", this.lastName);
        json.put("routes", this.routes.toJson());
        json.put("flightDate", this.flightDate.toString());
        if(isOption) {
        	json.put("nbrLuggage", this.nbrLuggage);
        }
        json.put("finalPrice", this.finalPrice);
        json.put("nbrTickets", this.nbrTickets );
        return json;
    }

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public int getNbrTickets() {
		return nbrTickets;
	}

	public void setNbrTickets(int nbrTickets) {
		this.nbrTickets = nbrTickets;
	}

}
