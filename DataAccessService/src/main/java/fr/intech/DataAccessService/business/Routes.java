package fr.intech.DataAccessService.business;

import org.json.JSONObject;

public class Routes {

	private Long idRoutes;
	
	private Airport departure;
	
	private Airport arrival;
	
	private int price;
	
	private int capacity;

	public Routes(Long id, Airport departure, Airport arrival, int price, int capacity) {
		super();
		this.idRoutes = id;
		this.departure = departure;
		this.arrival = arrival;
		this.price = price;
		this.capacity = capacity;
	}
	
	public Routes() {
		
	}

	public Long getId() {
		return idRoutes;
	}

	public void setId(Long id) {
		this.idRoutes = id;
	}

	public Airport getDeparture() {
		return departure;
	}

	public void setDeparture(Airport departure) {
		this.departure = departure;
	}

	public Airport getArrival() {
		return arrival;
	}

	public void setArrival(Airport arrival) {
		this.arrival = arrival;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("idRoutes", this.idRoutes);
        json.put("departure", this.departure.toJson());
        json.put("arrival", this.arrival.toJson());
        json.put("price", this.price);
        json.put("capacity", this.capacity);
        return json;
    }
	public static Routes fromJson(JSONObject json) {
        Long id = json.getLong("idRoutes");    
        Airport departure = Airport.fromJson(json.getJSONObject("departure"));
        Airport arrival = Airport.fromJson(json.getJSONObject("arrival"));
        int price = json.getInt("price");
        int capacity = json.getInt("capacity");

        return new Routes(id, departure, arrival, price, capacity);
    }
	
	
}
