package fr.intech.DataAccessService.business;

import org.json.JSONObject;

public class Airport {


	private String iata;
	
	private String name;

	public Airport(String iata, String name) {
		super();
		this.iata = iata;
		this.name = name;
	}
	
	public Airport() {
		
	}
	
	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static Airport fromJson(JSONObject json) {
        String name = json.getString("name");
        String iata = json.getString("iata");
        return new Airport(iata, name);
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("name", this.name);
        json.put("iata", this.iata);
        return json;
    }
}
