package fr.intech.External.API.mapper;

import fr.intech.External.API.dto.FlightInformation;
import fr.intech.External.API.dto.Flights;

public class FlightMapper {

	public static FlightInformation FlightToFlightInf(Flights flight, String date) {
		
		FlightInformation flightInf = new FlightInformation();
		
		
		flightInf.setDepartureIata(flight.getDeparture().get("iata"));
		flightInf.setArrivalIata(flight.getArrival().get("iata"));
		flightInf.setSeatsAvailable(flight.getCapacity());
		flightInf.setPrice(flight.getPrice());
		flightInf.setFlightDate(date);
		
		return flightInf;
		
	}
}
