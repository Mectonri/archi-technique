package fr.intech.External.API.dto;

public class FlightInformation {
	
	private String flightDate;
	
	private String departureIata;
	
	private String arrivalIata;
	
	private int seatsAvailable;
	
	private double price;

	public FlightInformation(String flightDate, String departureIata, String arrivalIata,
			int seatsAvailable, double price) {
		super();
		this.flightDate = flightDate;
		this.departureIata = departureIata;
		this.arrivalIata = arrivalIata;
		this.seatsAvailable = seatsAvailable;
		this.price = price;
	}
	
	public FlightInformation() {
		
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getDepartureIata() {
		return departureIata;
	}

	public void setDepartureIata(String departureIata) {
		this.departureIata = departureIata;
	}

	public String getArrivalIata() {
		return arrivalIata;
	}

	public void setArrivalIata(String arrivalIata) {
		this.arrivalIata = arrivalIata;
	}

	public int getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(int seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
}
