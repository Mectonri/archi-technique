package fr.intech.External.API.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import fr.intech.External.API.dto.FlightInformation;
import fr.intech.External.API.dto.Flights;
import fr.intech.External.API.mapper.FlightMapper;
import fr.intech.External.API.service.FlightService;

@Component
public class FlightServiceImpl implements FlightService {

	private final String URI = "http://localhost:8082/api/flight/";
	
	@Override
	public List<FlightInformation> getFlightByDate(String date) throws Exception {
		
		List<FlightInformation> flightInfList = new ArrayList<FlightInformation>();
		
		URL url = new URL(URI.concat(date));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();

		JSONArray jsonArr = new JSONArray(content.toString());
		
		for (int i = 0; i < jsonArr.length(); i++) {
			
            flightInfList.add(FlightMapper.FlightToFlightInf(Flights.fromJson(jsonArr.getJSONObject(i)), date));
            
        }
		
		return flightInfList;
	}

}
