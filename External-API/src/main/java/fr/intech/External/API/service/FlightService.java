package fr.intech.External.API.service;

import java.util.List;

import fr.intech.External.API.dto.FlightInformation;

public interface FlightService {

	List<FlightInformation> getFlightByDate(String date) throws Exception;

}
