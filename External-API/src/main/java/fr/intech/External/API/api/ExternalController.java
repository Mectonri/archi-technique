package fr.intech.External.API.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.intech.External.API.dto.FlightInformation;
import fr.intech.External.API.service.FlightService;

@RestController
@CrossOrigin
@RequestMapping("/controller")
public class ExternalController {
	
	@Autowired
	FlightService flightService;
	
	@GetMapping("/flights/{date}")
	public ResponseEntity<List<FlightInformation>> getFlightsByDate(@PathVariable(value = "date") String date) throws Exception{
		return ResponseEntity.ok(flightService.getFlightByDate(date));
	}
}
