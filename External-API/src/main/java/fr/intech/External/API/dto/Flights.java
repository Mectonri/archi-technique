package fr.intech.External.API.dto;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class Flights {

	private Long idRoutes;
	
	private Map<String, String> departure;
	
	private Map<String, String> arrival;
	
	private int price;
	
	private int capacity;
	
	private String company;
	
	public Flights(Long idRoutes, Map<String, String> departure, Map<String, String> arrival, int price, int capacity,
			String company) {
		super();
		this.idRoutes = idRoutes;
		this.departure = departure;
		this.arrival = arrival;
		this.price = price;
		this.capacity = capacity;
		this.company = company;
	}


	public Flights() {
		
	}


	public Long getIdRoutes() {
		return idRoutes;
	}



	public void setIdRoutes(Long idRoutes) {
		this.idRoutes = idRoutes;
	}



	public Map<String, String> getDeparture() {
		return departure;
	}



	public void setDeparture(Map<String, String> departure) {
		this.departure = departure;
	}



	public Map<String, String> getArrival() {
		return arrival;
	}



	public void setArrival(Map<String, String> arrival) {
		this.arrival = arrival;
	}



	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		this.price = price;
	}



	public int getCapacity() {
		return capacity;
	}



	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}



	public String getCompany() {
		return company;
	}



	public void setCompany(String company) {
		this.company = company;
	}
	
	public static Flights fromJson(JSONObject json) {
        Long id = json.getLong("id");
        JSONObject arrivalJson = json.getJSONObject("arrival");
        Map<String, String> arrivalMap = new HashMap<>();
        for (String key : arrivalJson.keySet()) {
            arrivalMap.put(key, arrivalJson.get(key).toString());
        }
        
        JSONObject departureJson = json.getJSONObject("departure");
        Map<String, String> departureMap = new HashMap<>();
        for (String key : departureJson.keySet()) {
        	departureMap.put(key, departureJson.get(key).toString());
        }
        
        int price = json.getInt("price");
        int capacity = json.getInt("capacity");
        

        return new Flights(id, departureMap, arrivalMap, price, capacity, "internal");
    }
	
}
