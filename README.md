## 1. Getting started

### 1.1. Récupérer les sources

```
$ git clone https://gitlab.com/Mectonri/archi-technique.git
```

### 1.2. Lancer l'application

```
$ mvn spring-boot:run
```