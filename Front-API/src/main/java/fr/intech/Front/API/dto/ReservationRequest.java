package fr.intech.Front.API.dto;

import org.json.JSONObject;

public class ReservationRequest {

	private String flightDate;
	
	private long idRoute;
	 
	public long getIdRoute() {
		return idRoute;
	}
	public void setIdRoute(long idRoute) {
		this.idRoute = idRoute;
	}
	public String getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}
	public long getRoutes() {
		return idRoute;
	}
	public void setRoutes(long routes) {
		this.idRoute = routes;
	}
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
        json.put("idRoute", this.idRoute);
        json.put("flightDate", this.flightDate);
        return json;
	}
}
