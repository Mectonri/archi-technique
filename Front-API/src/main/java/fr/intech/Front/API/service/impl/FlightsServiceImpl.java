package fr.intech.Front.API.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

import org.springframework.stereotype.Component;

import fr.intech.Front.API.dto.Flights;
import fr.intech.Front.API.service.FlightsService;

@Component
public class FlightsServiceImpl implements FlightsService{
	
	private static final String URL = "http://localhost:8082/api/flight/";

	
	@Override
	public List<Flights> getAllFlights() throws IOException {
		
		URL url = new URL(URL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();
		JSONArray jsonArr = new JSONArray(content.toString());
		
		List<Flights> flightList = new ArrayList<Flights>();
		for (int i = 0; i < jsonArr.length(); i++) {
			flightList.add(Flights.fromJson(jsonArr.getJSONObject(i)));
		}
		
		return flightList;
	}

}
