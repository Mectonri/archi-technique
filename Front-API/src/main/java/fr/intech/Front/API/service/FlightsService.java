package fr.intech.Front.API.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import fr.intech.Front.API.dto.Flights;

public interface FlightsService {
	
	public List<Flights> getAllFlights() throws MalformedURLException, IOException;
	
}
