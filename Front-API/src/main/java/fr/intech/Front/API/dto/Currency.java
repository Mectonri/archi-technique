package fr.intech.Front.API.dto;

import org.json.JSONObject;

public class Currency {
    String name;
    double rate;

    public Currency(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

	public static Currency fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		String name = jsonObject.getString("name");
		double rate = jsonObject.getDouble("rate");
		return new Currency(name, rate);
	}
}
