package fr.intech.Front.API.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.intech.Front.API.dto.ConfirmReservationRequest;
import fr.intech.Front.API.dto.Currency;
import fr.intech.Front.API.dto.Flights;
import fr.intech.Front.API.dto.ReservationRequest;
import fr.intech.Front.API.service.BookingService;
import fr.intech.Front.API.service.FlightsService;

@RestController
@CrossOrigin
@RequestMapping("/controller")
public class FrontAPIControler {

	@Autowired
	FlightsService flightService;
	
	@Autowired
	BookingService bookingService;
	
	@GetMapping("/")
	public ResponseEntity<List<Flights>> getAllRoutes() throws MalformedURLException, IOException{
		return ResponseEntity.ok(flightService.getAllFlights());
	}
	 
	 @GetMapping("/currencies")
	 public ResponseEntity<List<Currency>> getCurrencyRates () throws Exception{
	        return ResponseEntity.ok(bookingService.getCurrenciesRates());
	 }
	 
	 @PostMapping("/make")
	 public ResponseEntity<?> makeReservation(@RequestBody ReservationRequest request) {
		 
		try {
			Long response = bookingService.makeReservation(request);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			String errorMessage = "Capacity for this route on this date has been reached. Please choose a different date or route.";
			return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }		
		 
	 }
	 
	 @PostMapping("/confirm")
	 public ResponseEntity<?> confirmReservation(@RequestBody ConfirmReservationRequest request) throws Exception {
		 
		 bookingService.confirmReservation(request);
		 return new ResponseEntity<>(HttpStatus.OK);
		 
	 }
	
	 @PostMapping("/delete")
	 public ResponseEntity<?> deleteReservation(@RequestBody long idReserv) throws Exception {
		 
		 bookingService.deleteReservation(idReserv);
		 return new ResponseEntity<>(HttpStatus.OK);
		 
	 }
	
}
