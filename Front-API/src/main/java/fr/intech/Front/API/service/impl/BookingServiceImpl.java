package fr.intech.Front.API.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import fr.intech.Front.API.dto.ConfirmReservationRequest;
import fr.intech.Front.API.dto.Currency;
import fr.intech.Front.API.dto.ReservationRequest;
import fr.intech.Front.API.service.BookingService;

@Component
public class BookingServiceImpl implements BookingService{

	private final String URI = "http://localhost:8083/api/booking";

	@Override
	public List<Currency> getCurrenciesRates() throws Exception {
		
		URL url = new URL(URI.concat("/currencies"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();
		JSONArray jsonArr = new JSONArray(content.toString());
		
		ArrayList<Currency> currencyList = new ArrayList<Currency>();
		
		
		for (int i = 0; i < jsonArr.length(); i++) {
			currencyList.add(Currency.fromJson(jsonArr.getJSONObject(i)));
        }
		
		return currencyList;
	}

	@Override
	public Long makeReservation(ReservationRequest request) throws Exception {
		
		URL url = new URL(URI.concat("/make"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
		
		JSONObject jsonObject = request.toJson();
		String postContent = jsonObject.toString();		
		
		try(OutputStream os = con.getOutputStream()) {
		    byte[] input = postContent.getBytes("utf-8");
		    
		    os.write(input, 0, input.length);			
		}
		try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
	        String inputLine;
	        StringBuilder content = new StringBuilder();
	        while ((inputLine = in.readLine()) != null) {
	            content.append(inputLine);
	        }
	        in.close();
	        
	        return Long.parseLong(content.toString());
		 }
	}

	@Override
	public void confirmReservation(ConfirmReservationRequest request) throws Exception {
		URL url = new URL(URI.concat("/confirm"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
				
		JSONObject jsonObject = request.toJson();
		String postContent = jsonObject.toString();
		try(OutputStream os = con.getOutputStream()) {
			byte[] input = postContent.getBytes("utf-8");
			os.write(input, 0, input.length);
			}
		try(BufferedReader br = new BufferedReader(
				new InputStreamReader(con.getInputStream(), "utf-8"))) {
					StringBuilder response = new StringBuilder();
				    String responseLine = null;
				    while ((responseLine = br.readLine()) != null) {
				        response.append(responseLine.trim());
				    }
				    System.out.println(response.toString());
				}
	}

	@Override
	public void deleteReservation(long idReserv) throws Exception {
		URL url = new URL(URI.concat("/delete"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
				
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("idReserv", idReserv);
		String postContent = jsonObject.toString();
		try(OutputStream os = con.getOutputStream()) {
			byte[] input = postContent.getBytes("utf-8");
			os.write(input, 0, input.length);
			}
		try(BufferedReader br = new BufferedReader(
				new InputStreamReader(con.getInputStream(), "utf-8"))) {
					StringBuilder response = new StringBuilder();
				    String responseLine = null;
				    while ((responseLine = br.readLine()) != null) {
				        response.append(responseLine.trim());
				    }
				    System.out.println(response.toString());
				}
	}

}
