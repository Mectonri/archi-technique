package fr.intech.Front.API.service;

import java.util.List;

import fr.intech.Front.API.dto.ConfirmReservationRequest;
import fr.intech.Front.API.dto.Currency;
import fr.intech.Front.API.dto.ReservationRequest;

public interface BookingService {

	List<Currency> getCurrenciesRates() throws Exception;

	Long makeReservation(ReservationRequest request) throws Exception;

	void confirmReservation(ConfirmReservationRequest request) throws Exception;

	void deleteReservation(long idReserv) throws Exception;

}
