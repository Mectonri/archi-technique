package fr.intech.BookingEngine.api;

import fr.intech.BookingEngine.dto.ConfirmReservationRequest;
import fr.intech.BookingEngine.dto.Currency;
import fr.intech.BookingEngine.dto.ReservationRequest;
import fr.intech.BookingEngine.service.DataAccessService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    DataAccessService dataAccessService;
    
    @GetMapping("/currencies")
	public ResponseEntity<List<Currency>> getAllCurrencies() throws Exception{
		return ResponseEntity.ok(dataAccessService.getCurrenciesRates());
	}
    
    @PostMapping("/make")
	public ResponseEntity<Long> makeReservation(@RequestBody ReservationRequest request) throws Exception{
    	long reservationId = dataAccessService.makeReservation(request);
    	return ResponseEntity.ok(reservationId);
	}
    
    @PostMapping("/confirm")
   	public void confirmReservation(@RequestBody ConfirmReservationRequest request) throws Exception{
       	dataAccessService.confirmReservation(request);
       	
   	}
    
    @PostMapping("/delete")
	 public ResponseEntity<?> deleteReservation(@RequestBody long idReserv) throws Exception {
		 
    	dataAccessService.deleteReservation(idReserv);
		return new ResponseEntity<>(HttpStatus.OK);
		 
	 }
}
