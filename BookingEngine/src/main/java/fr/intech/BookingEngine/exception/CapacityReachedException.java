package fr.intech.BookingEngine.exception;

public class CapacityReachedException extends Exception{
	
	public CapacityReachedException(String errorMessage) {
		super(errorMessage);
	}
	
}
