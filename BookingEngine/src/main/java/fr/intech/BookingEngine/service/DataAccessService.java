package fr.intech.BookingEngine.service;

import java.util.List;

import fr.intech.BookingEngine.dto.ConfirmReservationRequest;
import fr.intech.BookingEngine.dto.Currency;
import fr.intech.BookingEngine.dto.ReservationRequest;

public interface DataAccessService {

	List<Currency> getCurrenciesRates() throws Exception;

	long makeReservation(ReservationRequest request) throws Exception;

	void confirmReservation(ConfirmReservationRequest request) throws Exception;

	void deleteReservation(long idReserv) throws Exception;;

}
