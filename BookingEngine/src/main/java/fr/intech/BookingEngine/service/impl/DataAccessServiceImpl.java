package fr.intech.BookingEngine.service.impl;

import fr.intech.BookingEngine.dto.ConfirmReservationRequest;
import fr.intech.BookingEngine.dto.Currency;
import fr.intech.BookingEngine.dto.ReservationRequest;
import fr.intech.BookingEngine.service.DataAccessService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Component
public class DataAccessServiceImpl implements DataAccessService {

	private final String URL = "http://localhost:8084/api/DAS/";


	@Override
	public List<Currency> getCurrenciesRates() throws Exception {
		
		URL url = new URL(URL.concat("currencies"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
		in.close();
		con.disconnect();
		JSONArray jsonArr = new JSONArray(content.toString());
		
		ArrayList<Currency> currencyList = new ArrayList<Currency>();
		
		
		for (int i = 0; i < jsonArr.length(); i++) {
			currencyList.add(Currency.fromJson(jsonArr.getJSONObject(i)));
        }
		
		return currencyList;
	}


	@Override
	public long makeReservation(ReservationRequest request) throws Exception {
		URL url = new URL(URL.concat("make"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        String postContent = request.toJson().toString();
        out.writeBytes(postContent);
        out.flush();
        out.close();

        int responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
        	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            
            return Long.parseLong(content.toString());
        } else {
            throw new Exception("Erreur lors de la réservation. Code de réponse : " + responseCode);
        }
	}


	@Override
	public void confirmReservation(ConfirmReservationRequest request) throws Exception {
		
		URL url = new URL(URL.concat("confirm"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        
        String postContent = request.toJson().toString();
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = postContent.getBytes("utf-8");
            os.write(input, 0, input.length);			
        }
        try(BufferedReader br = new BufferedReader(
        		  new InputStreamReader(con.getInputStream(), "utf-8"))) {
        		    StringBuilder response = new StringBuilder();
        		    String responseLine = null;
        		    while ((responseLine = br.readLine()) != null) {
        		        response.append(responseLine.trim());
        		    }
        		    System.out.println(response.toString());
        		}

	}


	@Override
	public void deleteReservation(long idReserv) throws Exception {
		URL url = new URL(URL.concat("/delete"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
				
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("idReserv", idReserv);
		String postContent = jsonObject.toString();
		try(OutputStream os = con.getOutputStream()) {
			byte[] input = postContent.getBytes("utf-8");
			os.write(input, 0, input.length);
			}
		try(BufferedReader br = new BufferedReader(
				new InputStreamReader(con.getInputStream(), "utf-8"))) {
					StringBuilder response = new StringBuilder();
				    String responseLine = null;
				    while ((responseLine = br.readLine()) != null) {
				        response.append(responseLine.trim());
				    }
				    System.out.println(response.toString());
				}
	}

}
