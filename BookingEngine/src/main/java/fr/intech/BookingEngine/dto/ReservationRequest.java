package fr.intech.BookingEngine.dto;

import org.json.JSONObject;

public class ReservationRequest {

	private long idRoute;
	
	private String flightDate;
	 
	public long getIdRoute() {
		return idRoute;
	}
	public void setIdRoute(long idRoute) {
		this.idRoute = idRoute;
	}
	public ReservationRequest(String flightDate, long idRoute) {
		super();
		this.flightDate = flightDate;
		this.idRoute = idRoute;
	}
	public ReservationRequest() {
		
	}
	public String getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}
	
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
        json.put("idRoute", this.idRoute);
        json.put("flightDate", this.flightDate);
        return json;
	}
}
