package fr.intech.BookingEngine.dto;

import org.json.JSONObject;

public class ConfirmReservationRequest {
	
	private long idReservation;
	
	private int nbrTickets;
	
	private int nbrLuggages;
	
	private double finalPrice;

	public ConfirmReservationRequest(long idReservation, int nbrTickets, int nbrLuggages, double finalPrice) {
		super();
		this.idReservation = idReservation;
		this.nbrTickets = nbrTickets;
		this.nbrLuggages = nbrLuggages;
		this.finalPrice = finalPrice;
	}
	
	public ConfirmReservationRequest() {
		
	}

	public long getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(long idReservation) {
		this.idReservation = idReservation;
	}

	public int getNbrTickets() {
		return nbrTickets;
	}

	public void setNbrTickets(int nbrTickets) {
		this.nbrTickets = nbrTickets;
	}

	public int getNbrLuggages() {
		return nbrLuggages;
	}

	public void setNbrLuggages(int nbrLuggages) {
		this.nbrLuggages = nbrLuggages;
	}

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public JSONObject toJson() {
		JSONObject json = new JSONObject();
        json.put("idReservation", this.idReservation);
        json.put("nbrTickets", this.nbrTickets);
        json.put("nbrLuggages", this.nbrLuggages);
        json.put("finalPrice", this.finalPrice);
        return json;
	}

}
